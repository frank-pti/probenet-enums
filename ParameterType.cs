/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !MICRO_FRAMEWORK
using System.ComponentModel;
#endif

namespace ProbeNet.Enums
{
    /// <summary>
    /// Parameter type.
    /// </summary>
    public enum ParameterType
    {
        /// <summary>
        /// Boolean parameter type. Parameters of this type can only have the value <c>true</c> or <c>false</c>.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("boolean")]
#endif
        Boolean,

        /// <summary>
        /// Number parameter type. Parameters of this type can only contain a numeric value.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("number")]
#endif
        Number,

        /// <summary>
        /// Enumeration parameter type. Parameters of this type can only contain a value of specified list of
        /// possible values.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("enumeration")]
#endif
        Enumeration
    }
}

