/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !MICRO_FRAMEWORK
using System.ComponentModel;
#endif

namespace ProbeNet.Enums
{
    /// <summary>
    /// Source type of a result.
    /// </summary>
    /// <remarks>
    /// A result is a numeric value which was measured or calculated in the process of a sequence. It can have a unit.
    /// There are two types of results: <c>Raw</c> and <c>Calculated</c>.
    /// </remarks>        
    public enum SourceType
    {
        /// <summary>
        /// Raw value.
        /// </summary>
        /// <remarks>
        /// The rsult was measured with a particular set of parameters which directly influenced the measurement
        /// process. A change to the parameters would require a new execution of the sequence in order to get the
        /// appropriate values. A conversion to a different unit does not change a raw value to a calculated value,
        /// because the conversion is not affected by any parameters.
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("raw")]
#endif
        Raw,

        /// <summary>
        /// Calculated value.
        /// </summary>
        /// <remarks>
        /// The result was calcuated based on raw values. It is affected by some parameters. When these parameters
        /// change, the result can be recalculated properly without executing the sequence again.
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("calculated")]
#endif
        Calculated
    }
}

