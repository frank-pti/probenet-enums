/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !MICRO_FRAMEWORK
using System.ComponentModel;
#endif

namespace ProbeNet.Enums
{
    /// <summary>
    /// Message type.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// The device description request.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("deviceDescriptionRequest")]
#endif
        DeviceDescriptionRequest,

        /// <summary>
        /// The device description.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("deviceDescription")]
#endif
        DeviceDescription,

        /// <summary>
        /// The sequence series metadata.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("sequenceSeriesMetadata")]
#endif
        SequenceSeriesMetadata,

        /// <summary>
        /// The sequence.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("sequence")]
#endif
        Sequence,

        /// <summary>
        /// The error.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("error")]
#endif
        Error,

        /// <summary>
        /// The measurement description request.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("measurementDescriptionRequest")]
#endif
        MeasurementDescriptionRequest,

        /// <summary>
        /// Measurement description.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("measurementDescription")]
#endif
        MeasurementDescription,

        /// <summary>
        /// Seqeunce description request.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("sequenceDescriptionRequest")]
#endif
        SequenceDescriptionRequest,

        /// <summary>
        /// Sequence description.
        /// </summary>
#if !MICRO_FRAMEWORK
        [Description("sequenceDescription")]
#endif
        SequenceDescription
    }
}

