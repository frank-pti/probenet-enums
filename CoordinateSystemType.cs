/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if !MICRO_FRAMEWORK
using System.ComponentModel;
#endif

namespace ProbeNet.Enums
{
    /// <summary>
    /// Coordinate system type.
    /// </summary>
    public enum CoordinateSystemType
    {
        /// <summary>
        /// The cartesian coordinate system type.
        /// </summary>
        /// <remarks>
        /// The cartesian coordinate system describes the points along two or three axes. The first value is the
        /// X value, the second one is the Y value, and in a three-dimensional system, the third one is the Z value.
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("cartesian")]
#endif
        Cartesian,

        /// <summary>
        /// The polar coordinate system type.
        /// </summary>
        /// <remarks>The polar coordinate system describes the points by the distance to the pole, the angle from
        /// the polar axis. The angles are represented in radians (0 to 2π).
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("polar")]
#endif
        Polar,

        /// <summary>
        /// The cylindrical coordinate system type.
        /// </summary>
        /// <remarks>
        /// The cylindrical coordinate system describes the points by the distance to the pole, the angle from the
        /// polar axis and the distance from the reference plane. The angle is expressed in radians (0 to 2π).
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("cylindrical")]
#endif
        Cylindrical,

        /// <summary>
        /// The spherical coordinate system type.
        /// </summary>
        /// <remarks>
        /// The spherical coordinate system describes the points by the distance to the pole, the angle from the
        /// polar axis and the angle from the reference plane. The angle is expressed in radians (0 to 2π).
        /// </remarks>
#if !MICRO_FRAMEWORK
        [Description("spherical")]
#endif
        Spherical
    }
}
