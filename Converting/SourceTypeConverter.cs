﻿/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ProbeNet.Enums.Converting
{
    /// <summary>
    /// Convert result source type
    /// </summary>
    public static class SourceTypeConverter
    {
        /// <summary>
        /// Serialized string of raw source type.
        /// </summary>
        internal const string RawString = "raw";
        /// <summary>
        /// Serialized string of calculated source type.
        /// </summary>
        internal const string CalculatedString = "calculated";

        /// <summary>
        /// Convert serialized value to enumeration value.
        /// </summary>
        /// <param name="serializedValue">The serialized value</param>
        /// <returns>The enum value</returns>
        public static SourceType Deserialize(string serializedValue)
        {
            switch (serializedValue) {
                case RawString:
                    return SourceType.Raw;
                case CalculatedString:
                    return SourceType.Calculated;
            }
            return SourceType.Raw;
        }
        /// <summary>
        /// Convert enumeration value to serialized string
        /// </summary>
        /// <param name="coordinateSystem">The enum value</param>
        /// <returns>The serialized value</returns>
        public static object Serialize(SourceType sourceType)
        {
            switch (sourceType) {
                case SourceType.Raw:
                    return RawString;
                case SourceType.Calculated:
                    return CalculatedString;
            }
            return null;
        }
    }
}
