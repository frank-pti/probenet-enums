/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ProbeNet.Enums.Converting
{
    /// <summary>
    /// Convert coordinate system type 
    /// </summary>
    public static class CoordinateSystemTypeConverter
    {
        /// <summary>
        /// The serialized string for cartesian type
        /// </summary>
        internal const string CartesianString = "cartesian";
        /// <summary>
        /// The serialized string for polar coordinate system type.
        /// </summary>
        internal const string PolarString = "polar";
        /// <summary>
        /// The serialized string for cylindrical coordinate system type.
        /// </summary>
        internal const string CylindricalString = "cylindrical";
        /// <summary>
        /// The serialized string for spherical coordinate system type.
        /// </summary>
        internal const string SpehericalString = "spherical";

        /// <summary>
        /// Convert serialized value to enumeration value.
        /// </summary>
        /// <param name="serializedValue">The serialized value</param>
        /// <returns>The enum value</returns>
        public static CoordinateSystemType Deserialize(string serializedValue)
        {
            switch (serializedValue)
            {
                case CartesianString:
                    return CoordinateSystemType.Cartesian;
                case PolarString:
                    return CoordinateSystemType.Polar;
                case CylindricalString:
                    return CoordinateSystemType.Cylindrical;
                case SpehericalString:
                    return CoordinateSystemType.Spherical;
            }
            return CoordinateSystemType.Cartesian;
        }

        /// <summary>
        /// Convert enumeration value to serialized string
        /// </summary>
        /// <param name="coordinateSystem">The enum value</param>
        /// <returns>The serialized value</returns>
        public static object Serialize(CoordinateSystemType coordinateSystem)
        {
            switch (coordinateSystem)
            {
                case CoordinateSystemType.Cartesian:
                    return CartesianString;
                case CoordinateSystemType.Polar:
                    return PolarString;
                case CoordinateSystemType.Cylindrical:
                    return CylindricalString;
                case CoordinateSystemType.Spherical:
                    return SpehericalString;
            }
            return null;
        }
    }
}
