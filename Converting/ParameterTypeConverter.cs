/*
* Enumeration types for using the ProbeNet Protocol with C#
* Copyright (C) Florian Marchl
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace ProbeNet.Enums.Converting
{
    /// <summary>
    /// Converts parameter type
    /// </summary>
    public static class ParameterTypeConverter
    {
        /// <summary>
        /// Serialized string of boolean parameter type.
        /// </summary>
        internal const string BooleanString = "boolean";
        /// <summary>
        /// Serialized string of number parameter type.
        /// </summary>
        internal const string NumberString = "number";
        /// <summary>
        /// Serialized string of enumeration parameter type.
        /// </summary>
        internal const string EnumerationString = "enumeration";

        /// <summary>
        /// Convert serialized value to enumeration value.
        /// </summary>
        /// <param name="serializedValue">The serialized value</param>
        /// <returns>The enum value</returns>
        public static ParameterType Deserialize(string serializedValue)
        {
            switch (serializedValue)
            {
                case BooleanString:
                    return ParameterType.Boolean;
                case NumberString:
                    return ParameterType.Number;
                case EnumerationString:
                    return ParameterType.Enumeration;
            }
            return ParameterType.Number;
        }

        /// <summary>
        /// Convert enumeration value to serialized string
        /// </summary>
        /// <param name="coordinateSystem">The enum value</param>
        /// <returns>The serialized value</returns>
        public static object Serialize(ParameterType parameterType)
        {
            switch (parameterType)
            {
                case ParameterType.Boolean:
                    return BooleanString;
                case ParameterType.Number:
                    return NumberString;
                case ParameterType.Enumeration:
                    return EnumerationString;
            }
            return null;
        }
    }
}
