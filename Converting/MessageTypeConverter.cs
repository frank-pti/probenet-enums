/*
 * Enumeration types for using the ProbeNet Protocol with C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ProbeNet.Enums.Converting
{
    /// <summary>
    /// Convert message type
    /// </summary>
    public static class MessageTypeConverter
    {
        /// <summary>
        /// The serialized string for device description request message type.
        /// </summary>
        internal const string DeviceDescriptionRequestString = "deviceDescriptionRequest";
        /// <summary>
        /// The serialized string for device descripton message type.
        /// </summary>
        internal const string DeviceDescriptionString = "deviceDescription";
        /// <summary>
        /// The serialized string for measurement descripton request message type.
        /// </summary>
        internal const string MeasurementDescriptionRequestString = "measurementDescriptionRequest";
        /// <summary>
        /// The serialized string for measurement descripton message type.
        /// </summary>
        internal const string MeasurementDescriptionString = "measurementDescription";
        /// <summary>
        /// The serialized string for sequence description request message type.
        /// </summary>
        internal const string SequenceDescriptionRequestString = "sequenceDescriptionRequest";
        /// <summary>
        /// The serialized string for sequence descripton message type.
        /// </summary>
        internal const string SequenceDescriptionString = "sequenceDescription";
        /// <summary>
        /// THe serialized string for sequence series metadata message type.
        /// </summary>
        internal const string SequenceSeriesMetadataString = "sequenceSeriesMetadata";        
        /// <summary>
        /// The serialized string for sequence message type.
        /// </summary>
        internal const string SequenceString = "sequence";
        /// <summary>
        /// The serialzed string for error message type.
        /// </summary>
        internal const string ErrorString = "error";

        /// <summary>
        /// Convert serialized value to enumeration value.
        /// </summary>
        /// <param name="serializedValue">The serialized value</param>
        /// <returns>The enum value</returns>
        public static MessageType Deserialize(string serializedValue)
        {
            switch (serializedValue) {
                case DeviceDescriptionRequestString:
                    return MessageType.DeviceDescriptionRequest;
                case DeviceDescriptionString:
                    return MessageType.DeviceDescription;
                case MeasurementDescriptionString:
                    return MessageType.MeasurementDescription;
                case MeasurementDescriptionRequestString:
                    return MessageType.MeasurementDescriptionRequest;
                case SequenceDescriptionRequestString:
                    return MessageType.SequenceDescriptionRequest;
                case SequenceDescriptionString:
                    return MessageType.SequenceDescription;
                case SequenceSeriesMetadataString:
                    return MessageType.SequenceSeriesMetadata;
                case SequenceString:
                    return MessageType.Sequence;
            }
            return MessageType.Error;
        }

        /// <summary>
        /// Convert enumeration value to serialized string
        /// </summary>
        /// <param name="coordinateSystem">The enum value</param>
        /// <returns>The serialized value</returns>
        public static object Serialize(MessageType messageType)
        {
            switch (messageType) {
                case MessageType.DeviceDescriptionRequest:
                    return BuildString(DeviceDescriptionRequestString);
                case MessageType.DeviceDescription:
                    return BuildString(DeviceDescriptionString);
                case MessageType.MeasurementDescriptionRequest:
                    return BuildString(MeasurementDescriptionRequestString);
                case MessageType.MeasurementDescription:
                    return BuildString(MeasurementDescriptionString);
                case MessageType.SequenceDescriptionRequest:
                    return BuildString(SequenceDescriptionRequestString);
                case MessageType.SequenceDescription:
                    return BuildString(SequenceDescriptionString);
                case MessageType.SequenceSeriesMetadata:
                    return BuildString(SequenceSeriesMetadataString);
                case MessageType.Sequence:
                    return BuildString(SequenceString);
            }
            return null;
        }

        private static string BuildString(string value)
        {
            return "\"" + value + "\"";
        }
    }
}
